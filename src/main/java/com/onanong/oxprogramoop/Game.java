/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onanong.oxprogramoop;

import java.util.Scanner;

/**
 *
 * @author onanongchanwiset
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    String answer;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: table at row and col is not empty!!");

        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void newGame() {
        table = new Table(playerX, playerO);
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (checkFinish()) {
                break;
            }
            table.switchPlayer();
        }
        this.showBye();
    }

    private boolean checkFinish() {
        if (table.isFinish()) {
            this.showTable();
            if (table.getWinner() == null) {
                System.out.println("Draw!!");
            } else {
                System.out.println(table.getWinner().getName() + " Win!!");
            }
            if (checkNewGame()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkNewGame() {
        System.out.println("Do you want to start a new game?(yes, no)");
        answer = kb.next();
        if ("yes".equals(answer)) {
            newGame();
        } else if ("no".equals(answer)) {
            return true;
        }
        return false;
    }

    public void showBye() {
        System.out.println("Bye bye ...");
    }
}
